package com.example.examen_final_videojuegos.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.examen_final_videojuegos.entities.Libro;

import java.util.List;

@Dao
public interface LibroDAO {
    @Query("SELECT * FROM Libros")
    List<Libro> getAll();

    @Insert
    void create(Libro libro);

    @Delete
    void delete(Libro libro);

}
