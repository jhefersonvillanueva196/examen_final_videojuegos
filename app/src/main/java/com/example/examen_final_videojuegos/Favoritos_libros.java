package com.example.examen_final_videojuegos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.example.examen_final_videojuegos.adapters.LibroAdapter;
import com.example.examen_final_videojuegos.dao.LibroDAO;
import com.example.examen_final_videojuegos.database.AppDatabase;
import com.example.examen_final_videojuegos.entities.Libro;
import com.google.gson.Gson;

import java.util.List;

public class Favoritos_libros extends AppCompatActivity {

    AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoritos_libros);
        db = AppDatabase.getDatabase(getApplicationContext());

        LibroDAO dao = db.libroDao();
        List<Libro> libros = dao.getAll();

        LibroAdapter adapter = new LibroAdapter(libros);

        RecyclerView rv = findViewById(R.id.rvLibros);
        rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rv.setHasFixedSize(true);
        rv.setAdapter(adapter);

        Log.i("DB", new Gson().toJson(libros));

    }
}