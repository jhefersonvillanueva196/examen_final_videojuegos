package com.example.examen_final_videojuegos.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.examen_final_videojuegos.dao.LibroDAO;
import com.example.examen_final_videojuegos.entities.Libro;

@Database(entities = {Libro.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract LibroDAO libroDao();

    public static AppDatabase getDatabase(Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, "Libros.db")
                .allowMainThreadQueries()
                .build();
    }
}

