package com.example.examen_final_videojuegos.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Libros")
public class Libro {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "caratula")
    public String caratula;

    @ColumnInfo(name = "titulo")
    public String titulo;

    @ColumnInfo(name = "resumen")
    public String resumen;

    @ColumnInfo(name = "donde_comprar")
    public String donde_comprar;

    @ColumnInfo(name = "favorito")
    public boolean favorito;

    public Libro() {

    }

    public Libro(int id, String caratula, String titulo, String resumen, String donde_comprar, boolean favorito) {
        this.id = id;
        this.caratula = caratula;
        this.titulo = titulo;
        this.resumen = resumen;
        this.donde_comprar = donde_comprar;
        this.favorito = favorito;
    }
}

