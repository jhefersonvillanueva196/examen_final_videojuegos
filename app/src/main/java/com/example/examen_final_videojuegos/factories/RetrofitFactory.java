package com.example.examen_final_videojuegos.factories;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitFactory {
    public static Retrofit build() {

        return new Retrofit.Builder()
                .baseUrl("https://62863682f0e8f0bb7c126992.mockapi.io/api/PS9/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }
}

