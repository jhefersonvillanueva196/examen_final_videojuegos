package com.example.examen_final_videojuegos.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.examen_final_videojuegos.DetalleLibro;
import com.example.examen_final_videojuegos.R;
import com.example.examen_final_videojuegos.entities.Libro;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;

public class LibroAdapter extends RecyclerView.Adapter<LibroAdapter.LibroViewHolder>{
    List<Libro> libros;
    public LibroAdapter(List<Libro> libros) {
        this.libros = libros;
    }

    @NonNull
    @Override
    public LibroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_item_libro, parent, false);
        return new LibroViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LibroViewHolder vh, int position) {

        View itemView = vh.itemView;

        Libro libro = libros.get(position);
        ImageView image = itemView.findViewById(R.id.image);
        TextView tvTitulo = itemView.findViewById(R.id.tvTitulo);

        Picasso.get().load(libro.caratula).into(image);
        tvTitulo.setText("Titulo: " + libro.titulo);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(itemView.getContext(), DetalleLibro.class);

                String contactJSON = new Gson().toJson(libro);
                intent.putExtra("LIBRO", contactJSON);

                itemView.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return libros.size();
    }

    class LibroViewHolder extends RecyclerView.ViewHolder implements AdapterView.OnItemClickListener {

        public LibroViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Libro contact = libros.get(i);
        }
    }

}
