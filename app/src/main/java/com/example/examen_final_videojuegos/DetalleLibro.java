package com.example.examen_final_videojuegos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.examen_final_videojuegos.dao.LibroDAO;
import com.example.examen_final_videojuegos.database.AppDatabase;
import com.example.examen_final_videojuegos.entities.Libro;
import com.example.examen_final_videojuegos.factories.RetrofitFactory;
import com.example.examen_final_videojuegos.services.LibroService;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DetalleLibro extends AppCompatActivity {

    AppDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_libro);

        String contactJson = getIntent().getStringExtra("LIBRO");
        Libro libro = new Gson().fromJson(contactJson, Libro.class);

        ImageView image = findViewById(R.id.image);

        TextView tvTitulo = findViewById(R.id.etTitulo);
        TextView tvResumen = findViewById(R.id.etResumen);
        Button editar = findViewById(R.id.btEditar);
        Button donde_comprar = findViewById(R.id.btDonde_Comprar);
        MaterialFavoriteButton favorito = findViewById(R.id.Fav);

        Picasso.get().load(libro.caratula).into(image);
        tvTitulo.setText(libro.titulo);
        tvResumen.setText(libro.resumen);

        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Libro animenew = new Libro();

                animenew.titulo = tvTitulo.getText().toString();
                animenew.resumen = tvResumen.getText().toString();
                animenew.caratula = libro.caratula;
                animenew.donde_comprar = libro.donde_comprar;

                Retrofit retrofit = RetrofitFactory.build();
                LibroService service = retrofit.create(LibroService.class);

                Call<Libro> call = service.update(libro.id, animenew);

                call.enqueue(new Callback<Libro>() {
                    @Override
                    public void onResponse(Call<Libro> call, Response<Libro> response) {
                        if(response.isSuccessful()) {
                            Log.e("VJ0906", "Edicion Correcta");
                        } else {
                            Log.i("VJ0906", "Error de aplicación");
                        }
                    }

                    @Override
                    public void onFailure(Call<Libro> call, Throwable t) {
                        Log.e("VJ0906", "No hubo conectividad con el servicio web");
                    }
                });

            }
        });

        donde_comprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                intent.putExtra("UBICACION", libro.donde_comprar);
                startActivity(intent);

            }
        });

        if(libro.favorito==true){
            favorito.setFavorite(true);
        }else{
            favorito.setFavorite(false);
        }

        favorito.setOnFavoriteChangeListener(new MaterialFavoriteButton.OnFavoriteChangeListener() {
            @Override
            public void onFavoriteChanged(MaterialFavoriteButton buttonView, boolean favorite) {
                if(favorite){
                    db = AppDatabase.getDatabase(getApplicationContext());
                    libro.favorito = favorite;
                    LibroDAO dao = db.libroDao();
                    dao.create(libro);
                    Log.i("Dato", "Se dio el cambio se agrego el elemento a la bd ");
                }else{
                    db = AppDatabase.getDatabase(getApplicationContext());
                    libro.favorito = favorite;
                    LibroDAO dao = db.libroDao();
                    dao.delete(libro);
                    Log.i("Dato", "Se dio el cambio se elimino el elemento de la bd ");
                }
            }
        });


    }
}