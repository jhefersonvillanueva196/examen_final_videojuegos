package com.example.examen_final_videojuegos;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.examen_final_videojuegos.entities.Libro;
import com.example.examen_final_videojuegos.factories.RetrofitFactory;
import com.example.examen_final_videojuegos.services.LibroService;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class FormLibro extends AppCompatActivity {
    static final int REQUEST_IMAGE_CAPTURE = 1000;
    static final int REQUEST_PICK_IMAGE = 1001;
    static String urlAPI = "https://leonars.imgur.com/all/";

    ImageView image;
    String imagen;
    EditText titulo,resumen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_libro);

        Button cargarImagen = findViewById(R.id.CargarImagen);

        image = findViewById(R.id.image);
        titulo = findViewById(R.id.etTitulo);
        resumen = findViewById(R.id.etResumen);


        Button Crear = findViewById(R.id.Crear);

        cargarImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tomarfotos();
            }
        });

        Crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validar()==true){
                    Libro libro = new Libro();
                    libro.titulo = titulo.getText().toString();
                    libro.resumen = resumen.getText().toString();
                    libro.donde_comprar = generarLonLat();
                    libro.favorito = false;


                    OkHttpClient client = new OkHttpClient().newBuilder()
                            .build();
                    MediaType mediaType = MediaType.parse("text/plain");
                    RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                            .addFormDataPart(libro.titulo, imagen)
                            .build();
                    Request request = new Request.Builder()
                            .url(urlAPI+libro.titulo)
                            .method("POST", body)
                            .addHeader("Authorization", "Client-ID 8bcc638875f89d9")
                            .build();

                    libro.caratula = urlAPI+libro.titulo;

                    Retrofit retrofit = RetrofitFactory.build();
                    LibroService service = retrofit.create(LibroService.class);

                    Call<Libro> call = service.create(libro);

                    call.enqueue(new Callback<Libro>() {
                        @Override
                        public void onResponse(Call<Libro> call, Response<Libro> response) {
                            if(response.isSuccessful()){
                                Log.i("Crear", "Se creo correctamente");
                            }else {
                                Log.i("Crear", "No creo correctamente");
                            }
                        }

                        @Override
                        public void onFailure(Call<Libro> call, Throwable t) {

                        }
                    });
                    Toast toast = Toast.makeText(getApplicationContext(), "Se creo correctamente", Toast.LENGTH_LONG);
                    toast.show();

                }else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Todos los campos son obligatorios", Toast.LENGTH_LONG);
                    toast.show();

                }

            }
        });
    }

    public boolean validar(){
        boolean band = true;
        String c1 = titulo.getText().toString();
        String c2 = resumen.getText().toString();
        if(c1.isEmpty()){
            band = false;
        }
        if(c2.isEmpty()){
            band = false;
        }

        return band;
    }

    private void tomarfotos() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        } catch (ActivityNotFoundException e) {
            // display error state to the user
        }
    }

    private String generarLonLat(){
        String cadena = "";
        double longitud = Math.random()*(180-(-180)+1)+(-180);
        double latitud = Math.random()*(90-(-90)+1)+(-90);
        cadena = longitud + "," + latitud;
        return cadena;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK && data != null) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            image.setImageBitmap(imageBitmap);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            imagen = Base64.encodeToString(byteArray, Base64.DEFAULT);
        }
        if (requestCode == REQUEST_PICK_IMAGE && resultCode == RESULT_OK && data != null) {
            try {
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                Bitmap imageBitmap = BitmapFactory.decodeStream(bufferedInputStream);
                image.setImageBitmap(imageBitmap);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream .toByteArray();
                imagen = Base64.encodeToString(byteArray, Base64.DEFAULT);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }


}